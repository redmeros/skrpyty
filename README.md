## Skrypty ##
Napisane przeze mnie do wewnętrznego/zewnętrznego użytku. Nie są ze sobą powiązane każdy plik zawiera oddzielny skrypt który powinien działać niezależnie od siebie. Języki skryptów zakładam, że będą niezależne od siebie. (są przecież ze sobą nie powiązane)

## Spis skryptów ##
1.  [sieci.bat](https://bitbucket.org/redmer/skrypty/raw/master/sieci.bat) - skrypt napisany dla Piotra Klecana z Atelier do szybkiego przełączania się między ustawieniami sieciowymi w domu i w biurze.

2. [backup/restore]() - skrypt (jeszcze nie napisany którego zadaniem jest backup wszelkich możliwych ustawień z komputera (mówimy o ustawieniach robota autocada i innych programów z których korzystam każdego dnia). Ścieżki/pliki które powinny być kopiowane:
    * ```%appdata%\Roaming\Autodesk Robot Structural Analysis 2016\CfgUsr``` - ustawienia użytkownika robota tutaj jest szablon PN-EN.cov

3. [baza_as.bat](https://bitbucket.org/redmer/skrypty/raw/master/baza_as.bat) - skrypt do stworzenia linku symbolicznego do serwera, (uwspólnienie bazy danych). - uruchamiać jako administrator
    * Uwaga zgodnie z [tym artykułem](https://superuser.com/questions/124679/how-do-i-create-a-link-in-windows-7-home-premium-as-a-regular-user?answertab=active#tab-top) możliwe jest włączenie skryptu bez uprawnień administratora dzięki temu można edytować ustawienia
	* Skrypt nie szuka wolnej litery tylko sam próbuje wpisać sobie literę t:
	* Skrypt do przetestowania na stacji roboczej w biurze