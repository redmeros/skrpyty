@ECHO off
cls
:start
ECHO.
ECHO 1. Biuro
ECHO 2. Dom
ECHO 3. Zamknij
set choice=
set /p choice=Wybierz numer sieci
if not '%choice%'=='' set choice=%choice:~0,1%
if '%choice%'=='1' goto con1
if '%choice%'=='2' goto autosearch
if '%choice%'=='3' goto end
ECHO "%choice%" Wpisz poprawny numer
ECHO.
goto start
:con1
ECHO Connecting Connection 1
netsh interface ip set address "Ethernet" static 192.168.223.207 255.255.255.0 192.168.223.1 1
netsh interface ip set dns "Ethernet" static 8.8.8.8
goto end

:autosearch
ECHO obtaining auto IP
netsh interface ip set address "Ethernet" dhcp
netsh interface ip set dns "Ethernet" static 8.8.8.8
ipconfig /renew "Ethernet"
goto end

:bye
ECHO BYE
goto end

:end