REM TUTAJ WPISAC SWOJE DANE!!!!!
SET USERNAME=#######
SET PASSWORD=#######
SET DOMAIN=DCSSC

@ECHO OFF

REM Katalog w ktorym znaduje sie Advance Steel 2016
SET AUTODESK_FOLDER=%PROGRAMDATA%\Autodesk
REM To sie zmienia tylko przy zmianie wersji AS
SET AS_FOLDER=Advance Steel 2016
REM Lokalizacja sieciowa bazy
SET AFTER_Y_PATH=02-DCS-DESIGN\DCS-AUTODESK\ADVANCE_STEEL\AS_2016_DB
REM Rozszerzenie jakie ma miec backup
SET BAK_EXT=bak
REM SCIEZKA DO Y:
SET PATH_TO_Y=\\192.168.0.249\archiwum
REM NAZWA UZYTKOWNIKA DO Y:

ECHO Skrypt do wlaczania/wylaczania uwspolnienia bazy danych
ECHO 1 - wlacz uwspolnienie (UWAGA ta opcja musi być uruchomiona z uprawnieniami ADMINISTRATORA)
ECHO 2 - wylacz uwspolnienie
ECHO 3 - wylacz uwspolnienie z kopiowaniem z serwera (do testow)
ECHO 0 - wylacz skrypt
ECHO.

SET /p command=Wybierz numer:
ECHO.
CALL :CASE_%command%
IF ERRORLEVEL 1 CALL :DEFAULT_CASE

ECHO GOTOWE
EXIT /B

:CASE_1
	ECHO CASE_1 proba wlaczenia uwspolnienia
	ECHO Proba stworzenia backupu
	ECHO.
	ECHO ZMIENIAM KATALOG "%AUTODESK_FOLDER%\%AS_FOLDER%" "%AS_FOLDER%.%BAK_EXT%"
	ECHO.
	
	IF EXIST "%AUTODESK_FOLDER%\%AS_FOLDER%.%BAK_EXT%" (
		SET /p answer=Katalog bat juz istnieje usunac?[y/n]
		IF %answer%==y (
			rmdir /S "%AUTODESK_FOLDER%\%AS_FOLDER%.%BAK_EXT%"
		) ELSE ( 
			GOTO :EOF
		)
	)
	
	rename "%AUTODESK_FOLDER%\%AS_FOLDER%" "%AS_FOLDER%.%BAK_EXT%"
	ECHO USTAWIAM LINK DO BAZY: "%AUTODESK_FOLDER%\%AS_FOLDER%" "%PATH_TO_Y%\%AFTER_Y_PATH%"
	ECHO.
	mklink /D "%AUTODESK_FOLDER%\%AS_FOLDER%" "%PATH_TO_Y%\%AFTER_Y_PATH%"
	GOTO END_CASE
:CASE_2
	ECHO CASE_2 proba wylaczenia uwspolnienia
	ECHO USUWAM "%AUTODESK_FOLDER%\%AS_FOLDER%"
	ECHO.
	rmdir "%AUTODESK_FOLDER%\%AS_FOLDER%"
	ECHO Zmieniam nazwe z "%AUTODESK_FOLDER%\%AS_FOLDER%.%BAK_EXT%" NA "%AS_FOLDER%"
	rename "%AUTODESK_FOLDER%\%AS_FOLDER%.%BAK_EXT%" "%AS_FOLDER%"
	ECHO.
	GOTO END_CASE
	
:CASE_3
	ECHO CASE_3 proba wylaczenia uwspolnienia z kopiowaniem bazy z serwera
	ECHO Podlaczenie lokalizacji sieciowej
	NET USE t: %PATH_TO_Y% /USER:%DOMAIN%\%USERNAME% %PASSWORD% /persistent:no
	robocopy "%PATH_TO_Y%\%AFTER_Y_PATH%" "%AUTODESK_FOLDER%\%AS_FOLDER%_tmp" /MIR
	ECHO.
	ECHO USUWAM "%AUTODESK_FOLDER%\%AS_FOLDER%"
	ECHO.
	rmdir "%AUTODESK_FOLDER%\%AS_FOLDER%"
	ECHO Zmieniam nazwe
	rename "%AUTODESK_FOLDER%\%AS_FOLDER%_tmp" "%AS_FOLDER%"
	NET USE /DELETE t:
	GOTO END_CASE
:DEFAULT_CASE
	ECHO WYSTAPIL BLAD PODCZAS PRZETWARZANIA
	GOTO :EOF
	
:END_CASE
	VER > NUL
	GOTO :EOF