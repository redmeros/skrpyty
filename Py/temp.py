from number_compiler.number_compiler import NumberCompiler
import re
str_numbers = """1160 
1161 
1162 
1163 
1164 
1165 
1166 
1167 
1168 
1169 
1647 
1648 
1649 
1650 
1651 
"""

m = re.search('(\d+)\s', str_numbers)
m2 = re.split('\s', str_numbers)
m3 = []
for a in m2:
    if len(a) == 0:
        continue
    if int(a) not in m3:
        m3.append(int(a))

#print (m3)
#nc = NumberCompiler(numbers)
#print(nc.compile())

nc = NumberCompiler(m3)
print(nc.compile())