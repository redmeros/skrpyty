"""
Moduł ma za zadanie się uruchomić i z każdego katalogu wziąć zdjęcia
fi przerobić je na pdf"""
import os
import img2pdf

def __main__():
    path = os.getcwd()
    for dirpath in os.listdir(path):
        if not os.path.isdir(dirpath):
            continue
        filenames = os.listdir(dirpath)
        inputlist = []
        for filename in filenames:
            inputlist.append(os.path.join(dirpath, filename))

        outpdfname = os.path.dirname(inputlist[0]) + ".pdf"
        with open(outpdfname, "wb") as fileh:
            fileh.write(img2pdf.convert(inputlist))

if __name__ == "__main__":
    __main__()
