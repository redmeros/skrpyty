from openpyxl import load_workbook
from shutil import copyfile
import os
import sys


startcell = (10, 9)
ext_to_find = ("dwg", "pdf")

def find(name, path):
    for root, dirs, files in os.walk(path):
        if name in files:
            return os.path.join(root, name)


def __main__():
    if (len(sys.argv) != 2):
        print("""Skrypt przyjmuje dokladnie jeden dodatkowy argument z
        nazwa pliku z lista z excela""")
        exit()

    listfilename = sys.argv[1]
    do_job(listfilename)


def do_job(listfilename):
    wb = load_workbook(listfilename, data_only=True)
    ws = wb.worksheets[0]

    cell = ws.cell(row = startcell[0], column=startcell[1])
    print(cell.value)

    for e in ext_to_find:
        try:
            print ("Tworze katalog '%s'" % e)
            os.mkdir(e)
        except FileExistsError:
            print ("Katalog '%s' istnieje, pomijam" % e)

    cr = startcell[0]
    cc = startcell[1]
    lastWasNull = False

    while(True):
        cell = ws.cell(row = cr, column=cc)
        fname = cell.value

        if (cell.value == None):
            if (lastWasNull == True):
                break
            else:
                lastWasNull = True
                cr += 1
                continue

        lastWasNull = False

        if cell.style == "NAGLOWEK":
            cr+=1
            continue
        cr += 1

        for ext in ext_to_find:
            toFind = "%s.%s" % (fname, ext)
            res = find(toFind, ".")
            if (res == None):
                print ("UWAGA! NIE ZNALAZLEM PLIKU: %s" % toFind)
                continue
            copyfile(res, os.path.join(".", ext, toFind))

if __name__ == "__main__":
    __main__()
