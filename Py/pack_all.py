#skrypt sluzy do pakowania katalogow w sposob zautomatyzowany

import os
import subprocess
packer_path = os.path.join("c:\\", "Program Files", "7-Zip", "7z.exe")

print ("Będę używał ścieżki packera: %s" % packer_path)

dirs = filter(os.path.isdir, os.listdir(os.getcwd()))
for dir in dirs:
    print ("Rozpoczynam pakowanie %s" % dir)
    subprocess.call([packer_path, "a", "%s.7z" % dir, dir])
