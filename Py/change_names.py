'''
SKRYPT STWORZONY DO ZMIAN NAZW KATALOGÓW NA SERWERZE NUMER XXX ZAMIENIANY
JEST NA NOWY NUMER PODANY PRZEZ UŻYTKOWNIKA
'''

import os.path
import sys

if len(sys.argv) == 1:
    print("Podales za malo argumentów. Usage: ")
    print("python change_dir_names [NOWYNUMER KATALOG]")
    exit()

newNumber = sys.argv[1]

print('--verbose' in sys.argv)

if len(sys.argv) == 3:
    rootDir = sys.argv[2]
else:
    rootDir = os.getcwd()

print("Nowy numer to:", newNumber)
print("Aktualny katalog to:", os.getcwd())
answer = input("Kontynuowac?[y/n]")

if answer != 'y':
    exit()

print("Zaczynamy...")

i = 1
for root, dirs, files in os.walk(rootDir, topdown=False):
    for name in dirs:
        OldPath = os.path.join(root, name)
        NewPath = os.path.join(root, name.replace("XXX", newNumber))
        msg = OldPath, " => ", NewPath;
        os.renames(OldPath, NewPath)
        if '--verbose' in sys.argv:
            print("".join(msg).encode(sys.stdout.encoding, errors="replace"))
        else:
            print(i)
        i = i + 1