import os
import sys
import argparse

class DCSProject:
    def __init__(self, name):
        self.name = name

    def ListDirs(self):
        rootDir = os.getcwd()

        for root, dirs, files in os.walk(rootDir, topdown=False):
            for dir in dirs:
                p = os.path.join(root, dir)
                print(p.replace(rootDir, ""))
    
    def ReadDirs(self, fileName=None):
        if fileName == None:
            fileName = 'dirs.lst'
        dirs = []
        with open(fileName, mode='r') as lines:
            for line in lines:
                dirs.append(line)
        return dirs

    def CreateDirs(self, newNumber, **kwargs):
        fileName = ""
        if 'filename' not in kwargs:
            fileName = None
        else:
            fileName =kwargs['filename']
        if 'dirs' not in kwargs:
            dirs = self.ReadDirs(fileName) 
        if 'rootDir' not in kwargs:
            kwargs['rootDir'] = os.getcwd()

        for dir in dirs:
            dirname = os.path.join(kwargs['rootDir'], dir.format(newNumber)).strip()
            print("Tworzę: %s" % dirname)
            try:
                if not os.path.exists(dirname):
                    os.makedirs(dirname)
            except Exception as ex:
                print ("Nie stworzono: %s, :%s" % (dirname, ex))
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Tworzy katalog ze strukturą projektu")
    parser.add_argument('--number', type=str, nargs=1, help='Numer Nowego projektu')
    parser.add_argument('--dirs', type=str, nargs='?', help='Plik z listą katalogów który zostanie użyty, domyślnie jest to `dirs.lst`', default="dirs.lst")

    args = parser.parse_args()
    a = vars(args)
    newNumber = a['number'][0]
    project = DCSProject("ICL DASZKI")
    project.CreateDirs(newNumber, filename=a['dirs'])
