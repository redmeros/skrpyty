import sys
import re


def test_data():
    data = '''
                  PUNKT       Warstwa: "0"
                              Obszar: Obszar modelu
                    Identyfikator = 1e1
                od punktu, X=  24.1481  Y=   -6.4705  Z=  30.0000
Kierunek wzrostu grubości względem LUW:
                   X=   0.2588  Y=  -0.9659  Z=   0.0000

                  PUNKT       Warstwa: "0"
                              Obszar: Obszar modelu
                    Identyfikator = 1d6
                od punktu, X=  24.1481  Y=   6.4705  Z=  20.0000

                  PUNKT       Warstwa: "0"
                              Obszar: Obszar modelu
                    Identyfikator = 1de
                od punktu, X=  47.5198  Y=  15.8387  Z=  35.0000
Kierunek wzrostu grubości względem LUW:
                   X=   0.2588  Y=  -0.9659  Z=   0.0000'''
    return data


# https://regex101.com/r/zM4mM6/1
def do_job(data):
    m = re.findall(', X=\s*(-?\d+\.\d+)\s+Y=\s*(-?\d+\.\d+)\s+Z=\s*(-?\d+\.\d+)', data)
    result = []
    for g in m:
        result.append('new Point3D({}, {}, {})'.format(g[0], g[1], g[2]))
    result = ",\n".join(result)
    return result

USAGE = """USAGE:
python AutocadPointListParser.py infilename outfilename"""
if len(sys.argv) < 2:
    print(USAGE)

infile = sys.argv[1]
if len(sys.argv) == 3:
    outfile = sys.argv[2]

data = ""
with open(infile, 'r') as contentfile:
    data = contentfile.read()

result = do_job(data)

if len(sys.argv) == 2:
    print(result)
else:
    f = open(outfile, 'w')
    f.write(result)