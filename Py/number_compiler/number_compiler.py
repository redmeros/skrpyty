import re

class NumberCompiler:
    delimeter = "-"

    def __init__(self, numbers = None):
        self.Numbers = numbers

    def add_text(self, text):
        pass
    
    def decompile(self, text):
        reg = "(\d+-\d+)|(\d+)"
        matches = re.findall(reg, text)
        reg_result = []
        done_result = []

        for r in matches:
            for w in r:
                if len(w) > 0:
                    reg_result.append(w)
        
        for r in reg_result:
            if "-" in r:
                m = r.split("-")
                first = int(m[0])
                last = int(m[1])
                for i in range(first, last+1, 1):
                    done_result.append(i)
            else:
                done_result.append(int(r))
        
        print (done_result)

    def compile(self):
        self.Numbers = sorted(self.Numbers)

        ret = ''
        ranges = []

        current_range = []

        for i,current in enumerate(self.Numbers):
            if len(current_range) == 0:
                current_range.append(current)
                continue

            if current - current_range[-1] == 1:
                current_range.append(current)
            else:
                ranges.append(current_range)
                current_range = [current]
        ranges.append(current_range)
        strings = []

        for r in ranges:
            if len(r) == 1:
                strings.append ('%i' % r[0])
            else:
                strings.append('%i%s%i' % (r[0], '-', r[-1]))
        
        return " ".join(strings)


if __name__=="__main__":
    txt = """1565-1570 1571-1576 1496 1510-1512 1559-1564 1577-1584 1645
    1355 1375-1378 1405-1421 1437 1462-1466 1492-1493
    1380 1383 1392 1395 1423-1428 1433 1461 1494-1495 1508 1558 1594-1595 
    1013-1015 1024-1026 1028 1036 1063-1066 1084 1091-1092 1094-1095 1119 1137-1138 1201-1202 1361-1362 1364 1366 1373 1379 1382 1384-1387 1390 1396 1471-1472 1481 1488 1491 1507 1509 1522-1523 1525 1642-1644 
    1007,1438, 1473, 1590-1592 
    1000 1008 1021 1039 1048 1053 1076 1079-1081 1136 1139-1150 1254-1258 1262-1266 1268-1270 
    1271-1278 
    1356-1360 1363 1365 1367-1372 1374 1388-1389 1391 1393-1394 1397-1404 1422 1429-1430 1455 1457 
    1431 1434-1436 1439-1445 1447-1448 1448-1454 1456 
    1458-1460 1467-1470 1475 1479-1480 1482-1487 1489 1498 1502 1505-1506 1514 1516-1517 
    1497 1499-1501 1503-1504 1513 1518 1526-1528  
    1519 1521 1524 1529-1533 
    1534-1536 1537-1539 1540-1542 1543-1551 1552-1557 1585-1587 1596-1603 
    1588-1589 1606-1608 1604 1605 1609-1611 1612-1614 1615-1617 1618-1620 1621-1622 
    1003 1005 1011 1031 1061 1062 1090 1104-1105 1112 1121 1127-1128 1157 1193 
    1197-1198 1200 1208 1217 1237 1199 1238-1239 1250 1352-1353 
    1001-1002 1004 1006 1009-1010 1012 1016-1020 1022-1023 1027 1029-1030 1032-1033 1037-1038 1040-1047 1049-1052 1054-1055 
    1056-1057 1060 1067-1075 1082-1083 1085-1089 1093 1096-1101 
    1102-1103 1107-1108 1110 1113-1115 1117-1118 1120 1122-1126 1130 1133-1135 1153-1156 1158-1159 1170-1171 
    1172-1188 1189 1192 1194-1195 1203-1207 1209-1210 1212-1216 1218-1236 1240 1242-1249 1252 
    1253 1259 1279-1299 1301-1307 1308-1324 1325-1351 """
    nc = NumberCompiler()
    nc.decompile(txt)
