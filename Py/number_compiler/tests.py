from number_compiler import NumberCompiler
import unittest

class test_number_compiler(unittest.TestCase):

    input = [
        ([1356, 1357, 1358, 1359, 1360, 1363, 1365, 1367 ,1368,
    1369, 1370, 1371, 1372, 1374 ,1388 ,1389 ,1391 ,1393 ,1394 ,1397 ,
    1398 ,1399 ,1400 ,1401 ,1402 ,1403 ,1404 ,1422 ,1429 ,1430 ,1455 ,
    1457],
        "1356-1360 1363 1365 1367-1372 1374 1388-1389 1391 1393-1394 1397-1404 1422 1429-1430 1455 1457"
        )
    ]
    
    def test_results(self):
        for res in self.input:
            nc = NumberCompiler(res[0])
            self.assertEqual(nc.compile(), res[1])

if __name__ == '__main__':
    unittest.main()