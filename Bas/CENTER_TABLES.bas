Sub CENTER_TABLES()
'
' CENTER_TABLES WORD_MAKRO
'
'
Dim tbl As Table
Dim stl As Style
For Each tbl In ActiveDocument.Tables
    tbl.PreferredWidthType = wdPreferredWidthPercent
    tbl.PreferredWidth = 98
    tbl.Rows.Alignment = wdAlignRowCenter
Next
End Sub

